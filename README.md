# Toy Merchant Sockets

> A distributed system
Server and client communication over Java sockets

### Project structure
    .
    ├── client
    │   ├── ClientApp            # Client applicatiion UI class
    │   ├── ClientProtocol       # Manages business logic interaction in reaction to the server
    │   └── SocketClient         # Handles the network communication between the client and server
    ├── models
    │   ├── ThankYouMessage      # Thank you message data model
    │   ├── ToyDetails           # Toy details data model
    │   ├── ToyInformation       # Contains infromation pertaining the toy
    │   ├── ToyManufucterer      # Handles the toy manufucterer infromation 
    │   └── UIInteractor         # Allows other classes to manipulate the UI
    ├── server
    │   ├── ServerApp            # Server application UI calss 
    │   ├── ServerProtocol       # Manages business logic interactions in response to the client
    │   └── SocketServer         # Manages network communication between the server and the client
    ├── utils
    │   ├── Actions              # Actions that can be taken between the server and the client.
    │   ├── Config               # Application configuration 
    │   ├── FileLogger           # Stores logs to a file
    │   └── NumberTextFiled      # Number field class based on a text field   
    └── README.md                # You're Here

#### Actions that can be taken between the server and the client.
- Actions#UNKNOWN
- Actions#CONNECT
- Actions#OK
- Actions#SEND_TOY_DETAILS
- Actions#SEND_TOY_INFO
- Actions#SEND_TOY_MANUFACTURER
- Actions#SEND_THANKS
- Actions#SEND_ALL

