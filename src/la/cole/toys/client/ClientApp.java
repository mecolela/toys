package la.cole.toys.client;

import la.cole.toys.models.*;
import la.cole.toys.utils.NumberTextField;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * Application class for the client UI
 */
public class ClientApp extends Application implements UIInteractor {
    private final SocketClient socketClient = new SocketClient(this);
    private TextArea txtLog;
    private Boolean isConnected = false;

    private NumberTextField txtToyInfoCode;
    private TextField txtToyInfoName;
    private TextField txtToyDetailsName;
    private TextField txtToyDetailsDescription;
    private NumberTextField txtToyDetailsPrice;
    private DatePicker txtDateOfManufacture;
    private TextField txtBatchNumber;
    private TextField txtManufacturerName;
    private TextField txtStreetAddress;
    private TextField txtZipCode;
    private TextField txtCountry;
    private TextField txtMessage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Button to connect to server
        Button btnConnect = new Button("Connect");
        // Run on separate thread to prevent the while loop from hanging the main thread
        btnConnect.setOnAction(e -> {
            if (!isConnected) {
                new Thread(() -> {
                    isConnected = true;
                    socketClient.startListening();
                }).start();
            }
        });

        // Main content
        VBox toyDetailsContainer = getToyDetailsContainer();
        VBox toyInformationContainer = getToyInformationContainer();
        VBox toyManufacturerContainer = getToyManufacturerContainer();
        VBox thanksContainer = getThanksContainer();
        HBox hBoxCenter = new HBox();
        hBoxCenter.getChildren().addAll(toyDetailsContainer, toyInformationContainer, toyManufacturerContainer, thanksContainer);

        Button btnSendAll = new Button("Send all");
        btnSendAll.setOnAction(e -> sendAll());

        // Log Area
        txtLog = new TextArea();
        txtLog.setEditable(false);

        // Button to clear log
        Button btnClearLog = new Button("Clear");
        btnClearLog.setOnAction(e -> txtLog.clear());

        // Container
        VBox container = new VBox(10);
        container.setPadding(new Insets(10));
        container.getChildren().addAll(btnConnect, hBoxCenter, btnSendAll, txtLog, btnClearLog);

        // Window
        Scene scene = new Scene(container);
        primaryStage.setTitle("DS - Client - Toy Merchant");
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.show();
        primaryStage.setMinWidth(primaryStage.getWidth());
        primaryStage.setMinHeight(primaryStage.getHeight());

        // Override close
        primaryStage.setOnCloseRequest(e -> {
            e.consume();
            primaryStage.close();
            System.exit(0);
        });
    }

    private VBox getToyDetailsContainer() {
        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(10));

        Label label = new Label("Toy Details");

        txtToyInfoCode = new NumberTextField(1);
        txtToyInfoCode.setPromptText("Code");

        txtToyInfoName = new TextField("Toy 1");
        txtToyInfoName.setPromptText("Name");

        Button btn = new Button("Send");
        btn.setOnAction(e -> {
            if (!isConnected) {
                log("Not connected to server");
                return;
            }
            ToyDetails toyDetails = new ToyDetails(Integer.parseInt(txtToyInfoCode.getText()), txtToyInfoName.getText());
            socketClient.protocol.sendToyDetails(toyDetails);
        });

        vBox.getChildren().addAll(label, txtToyInfoCode, txtToyInfoName, btn);

        return vBox;
    }

    private VBox getToyInformationContainer() {
        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(10));

        Label label = new Label("Toy Information");

        txtToyDetailsName = new TextField("Toy 1");
        txtToyDetailsName.setPromptText("Name");

        txtToyDetailsDescription = new TextField("A blue toy");
        txtToyDetailsDescription.setPromptText("Description");

        txtToyDetailsPrice = new NumberTextField(100);
        txtToyDetailsPrice.setPromptText("Price");


        txtDateOfManufacture = new DatePicker(LocalDate.now());
        txtDateOfManufacture.setPromptText("Date of Manufacture");

        txtBatchNumber = new TextField("B0-11");
        txtBatchNumber.setPromptText("Batch Number");

        Button btn = new Button("Send");
        btn.setOnAction(e -> {
            if (!isConnected) {
                log("Not connected to server");
                return;
            }
            String manufacture = txtDateOfManufacture.getValue().format(DateTimeFormatter.ISO_DATE);
            ToyInformation toyInformation = new ToyInformation(txtToyDetailsName.getText(), txtToyDetailsDescription.getText(), Double.parseDouble(txtToyDetailsPrice.getText()), manufacture, txtBatchNumber.getText());
            socketClient.protocol.sendToyInfo(toyInformation);
        });

        vBox.getChildren().addAll(label, txtToyDetailsName, txtToyDetailsDescription, txtToyDetailsPrice, txtDateOfManufacture, txtBatchNumber, btn);

        return vBox;
    }

    private VBox getToyManufacturerContainer() {
        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(10));

        Label label = new Label("Toy Manufacturer");

        txtManufacturerName = new TextField("Manufacturer 1");
        txtManufacturerName.setPromptText("Name");

        txtStreetAddress = new TextField("Unnamed Road");
        txtStreetAddress.setPromptText("Street Address");

        txtZipCode = new TextField("00100");
        txtZipCode.setPromptText("Zip Code");

        txtCountry = new TextField("KE");
        txtCountry.setPromptText("Country");

        Button btn = new Button("Send");
        btn.setOnAction(e -> {
            if (!isConnected) {
                log("Not connected to server");
                return;
            }
            ToyManufacturer toyManufacturer = new ToyManufacturer(txtManufacturerName.getText(), txtStreetAddress.getText(), txtZipCode.getText(), txtCountry.getText());
            socketClient.protocol.sendToyManufacturer(toyManufacturer);
        });

        vBox.getChildren().addAll(label, txtManufacturerName, txtStreetAddress, txtZipCode, txtCountry, btn);

        return vBox;
    }

    private VBox getThanksContainer() {
        VBox vBox = new VBox(10);
        vBox.setPadding(new Insets(10));

        Label label = new Label("Send Thanks");

        txtMessage = new TextField("Thank You");
        txtMessage.setPromptText("Message");

        Button btn = new Button("Send");
        btn.setOnAction(e -> {
            if (!isConnected) {
                log("Not connected to server");
                return;
            }
            String code = UUID.randomUUID().toString().split("-")[0];
            ThankYouMessage thankYouMessage = new ThankYouMessage(code, txtMessage.getText());
            socketClient.protocol.sendThanks(thankYouMessage);
        });

        vBox.getChildren().addAll(label, txtMessage, btn);

        return vBox;
    }

    private void sendAll() {
        ToyDetails toyDetails = new ToyDetails(Integer.parseInt(txtToyInfoCode.getText()), txtToyInfoName.getText());
        String dateOfManufacture = txtDateOfManufacture.getValue().format(DateTimeFormatter.ISO_DATE);
        ToyInformation toyInformation = new ToyInformation(txtToyDetailsName.getText(), txtToyDetailsDescription.getText(), Double.parseDouble(txtToyDetailsPrice.getText()), dateOfManufacture, txtBatchNumber.getText());
        ToyManufacturer toyManufacturer = new ToyManufacturer(txtManufacturerName.getText(), txtStreetAddress.getText(), txtZipCode.getText(), txtCountry.getText());
        String code = UUID.randomUUID().toString().split("-")[0];
        ThankYouMessage thankYouMessage = new ThankYouMessage(code, txtMessage.getText());

        if (!isConnected) {
            log("Not connected to server");
            return;
        }
        socketClient.protocol.sendAll(toyDetails, toyInformation, toyManufacturer, thankYouMessage);
    }

    private void logToScreen(String text) {
        txtLog.appendText(text);
        txtLog.appendText("\n");
    }

    @Override
    public void log(String text) {
        logToScreen(text);
    }

}
