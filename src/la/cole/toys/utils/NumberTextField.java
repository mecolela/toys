package la.cole.toys.utils;

import javafx.scene.control.TextField;

/**
 * Number field class based on a text field
 */
public class NumberTextField extends TextField {
    public NumberTextField(int text) {
        super(Integer.toString(text));
    }

    @Override
    public void replaceText(int start, int end, String text) {
        if (validate(text)) {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text) {
        if (validate(text)) {
            super.replaceSelection(text);
        }
    }

    private boolean validate(String text) {
        return text.matches("[0-9]*");
    }
}
