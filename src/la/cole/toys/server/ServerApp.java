package la.cole.toys.server;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import la.cole.toys.models.UIInteractor;

public class ServerApp extends Application implements UIInteractor {
    private final SocketServer socketServer = new SocketServer(this);
    private TextArea txtLog;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Run on separate thread to prevent the while loop from hanging the main thread
        new Thread(socketServer::startListening).start();

        // Log Area
        txtLog = new TextArea();
        txtLog.setEditable(false);

        // Button to clear log
        Button btnClearLog = new Button("Clear");
        btnClearLog.setOnAction(e -> txtLog.clear());

        // Container
        VBox container = new VBox(10);
        container.setPadding(new Insets(10));
        container.getChildren().addAll(txtLog, btnClearLog);

        // Window
        Scene scene = new Scene(container);
        primaryStage.setTitle("DS - Server - Toy Merchant");
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.show();
        primaryStage.setMinWidth(primaryStage.getWidth());
        primaryStage.setMinHeight(primaryStage.getHeight());

        // Override close
        primaryStage.setOnCloseRequest(e -> {
            e.consume();
            primaryStage.close();
            System.exit(0);
        });
    }

    @Override
    public void log(String text) {
        logToScreen(text);
    }

    private void logToScreen(String text) {
        txtLog.appendText(text);
        txtLog.appendText("\n");
    }

}
