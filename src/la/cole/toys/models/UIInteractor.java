package la.cole.toys.models;

/**
 * Allows other classes to manipulate the UI.
 */
public interface UIInteractor {
    void log(String text);
}
