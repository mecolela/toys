package la.cole.toys.models;

/**
 * Toy information data model
 */
public class ToyInformation {
    private final String name;
    private final String description;
    private final double price;
    private final String dateOfManufacture;
    private final String batchNumber;

    public ToyInformation(String name, String description, double price, String dateOfManufacture, String batchNumber) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.dateOfManufacture = dateOfManufacture;
        this.batchNumber = batchNumber;
    }

    @Override
    public String toString() {
        return "ToyInformation{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", dateOfManufacture='" + dateOfManufacture + '\'' +
                ", batchNumber='" + batchNumber + '\'' +
                '}';
    }

    public static String serialize(ToyInformation input) {
        return String.format("%s;%s;%.2f;%s;%s", input.name, input.description, input.price, input.dateOfManufacture, input.batchNumber);
    }

    public static ToyInformation deserialize(String input) {
        String[] splits = input.split(";");
        return new ToyInformation(splits[0], splits[1], Double.parseDouble(splits[2]), splits[3], splits[4]);
    }
}
