package la.cole.toys.models;

/**
 * Toy details data model
 */
public class ToyDetails {
    private final int code;
    private final String name;

    public ToyDetails(int code, String name) {
        this.code = code;
        this.name = name;
    }

    @Override
    public String toString() {
        return "ToyDetails{" +
                "code=" + code +
                ", name='" + name + '\'' +
                '}';
    }

    public static String serialize(ToyDetails input) {
        return String.format("%d;%s", input.code, input.name);
    }

    public static ToyDetails deserialize(String input) {
        String[] splits = input.split(";");
        return new ToyDetails(Integer.parseInt(splits[0]), splits[1]);
    }
}
